package de.webtrekk.algorithm.fader;

import java.util.ArrayList;

import de.webtrekk.dfo.Calcfc;

public class ProbabilityForAllUsers implements Calcfc {

    private ArrayList<ArrayList<Integer>> views;

    private ArrayList<ArrayList<Integer>> buys;

    private ArrayList<ArrayList<Integer>> views_cumulative;

    private ArrayList<ArrayList<Integer>> buys_cumulative;

    private int numberOfUsers;

    public ProbabilityForAllUsers(ArrayList<ArrayList<Integer>> views, ArrayList<ArrayList<Integer>> buys,
            ArrayList<ArrayList<Integer>> views_cumulative, ArrayList<ArrayList<Integer>> buys_cumulative) {
        this.views = views;
        this.buys = buys;
        this.views_cumulative = views_cumulative;
        this.buys_cumulative = buys_cumulative;
        this.numberOfUsers = views.size();
    }

    public double Compute(int n, int m, double[] x, double[] con) {
        
        con[0] = x[0];
        con[1] = x[1];
        con[2] = x[2];
        con[3] = x[4];
        con[4] = (x[5]) * (1 - x[5]);
        ArrayList<Double> resultList = new ArrayList<Double>();

        for (int i = 0; i < numberOfUsers; i++) {

            double resultRow = probabilityForSingleUser(views.get(i), buys.get(i), views_cumulative.get(i),
                    buys_cumulative.get(i), x);
            resultList.add(resultRow);
            //System.out.println("row " + i + " computed");
        }

        double result = 0d;

        for (Double d : resultList) {
            result += d;
        }

        return -(result);

    }

    private double probabilityForSingleUser(ArrayList<Integer> current_views, ArrayList<Integer> current_buys,
            ArrayList<Integer> current_views_cumulative, ArrayList<Integer> current_buys_cumulative, double[] x) {

        double rv = x[0];
        double rt = x[1];
        double mu = x[2];
        double psi = x[3];
        double k = x[4];
        double pi = x[5];

        int len = current_views.size();
        ArrayList<Double> resultList = new ArrayList<Double>();
        int lastPurchase = -1;
        
        for (int i = 0; i < len; i++) {
            if (current_views.get(i) == 1) {
                double currentProb = 0d;
                if (current_buys_cumulative.get(i) == 0) {
                    double numeratorZero = rv + muZeroSum(1, i, k, mu);
                    currentProb = ((1 - pi) + pi
                            * ((numeratorZero) / (numeratorZero + rt + current_views_cumulative.get(i))));
                } else {
                    double numerator = rv + muZeroSum(lastPurchase + 1, i, k, mu);
                    currentProb = ((numerator + current_buys_cumulative.get(i)) / (numerator + rt * Math.exp(psi * current_buys_cumulative.get(i)) + current_views_cumulative
                            .get(i)));
                }
                
                if (current_buys.get(i) == 1) {
                    resultList.add(currentProb);
                    lastPurchase = i;
                } else {
                    resultList.add(1.0 - currentProb);
                }
                
            }
        }

        double result = 0d;

        for (Double d : resultList) {
            if(d != 0.0) {
                result += Math.log(d);
            }
            
        }

        return result;

    }

    private double muZeroSum(int start, int end, double k, double mu) {

        double result = Math.pow(k, start);
        int exponent = start + 1;
        
        while(exponent <= end) {
            result += k * result;
            exponent++;
        }
        
        return mu * result;
       
    }

}
