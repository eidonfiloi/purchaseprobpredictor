package de.webtrekk.purchase.probability.predictor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PreprocessMatrixData {

    private final String VIEWS_PATH = "C:\\Users\\pto\\RecoEngine\\RecoAlgorithm\\data2_views_new.csv";

    private final String BUYS_PATH = "C:\\Users\\pto\\RecoEngine\\RecoAlgorithm\\data2_buys_new.csv";

    private final String VIEWS_CUMULATIVE_PATH = "C:\\Users\\pto\\RecoEngine\\RecoAlgorithm\\data2_views_cumulative_new.csv";

    private final String BUYS_CUMULATIVE_PATH = "C:\\Users\\pto\\RecoEngine\\RecoAlgorithm\\data2_buys_cumulative_new.csv";

    private final String DELIMETER = ",";

    private int totalTimeInDays;

    private int totalUsers;

    private int trainTimeInDays;

    private ArrayList<ArrayList<Integer>> views_train;

    private ArrayList<ArrayList<Integer>> views_train_cumulative;

    private ArrayList<ArrayList<Integer>> buys_train;

    private ArrayList<ArrayList<Integer>> buys_train_cumulative;

    public PreprocessMatrixData(int totalTimeInDays, int trainTimeInDays) throws IOException {
        this.totalTimeInDays = totalTimeInDays;
        this.trainTimeInDays = trainTimeInDays;
        this.views_train = buildMatrix(VIEWS_PATH, this.trainTimeInDays);
        this.views_train_cumulative = buildMatrix(VIEWS_CUMULATIVE_PATH, this.trainTimeInDays);
        this.buys_train = buildMatrix(BUYS_PATH, this.trainTimeInDays);
        this.buys_train_cumulative = buildMatrix(BUYS_CUMULATIVE_PATH, this.trainTimeInDays);
        this.totalUsers = this.views_train.size();
    }

    public ArrayList<ArrayList<Integer>> getViews_train() {
        return views_train;
    }

    public ArrayList<ArrayList<Integer>> getViews_train_cumulative() {
        return views_train_cumulative;
    }

    public ArrayList<ArrayList<Integer>> getBuys_train() {
        return buys_train;
    }

    public ArrayList<ArrayList<Integer>> getBuys_train_cumulative() {
        return buys_train_cumulative;
    }

    public int getTotalUsers() {
        return totalUsers;
    }

    private ArrayList<ArrayList<Integer>> buildMatrix(String path, int len) throws IOException {

        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();

        BufferedReader reader = new BufferedReader(new FileReader(path));

        String line;
        int row = 0;
        while ((line = reader.readLine()) != null) {

            String[] lineStr = line.split(DELIMETER);
            ArrayList<Integer> rowList = new ArrayList<Integer>();
            for (int i = 1; i <= len; i++) {
                rowList.add(Integer.valueOf(lineStr[i]));
            }
            result.add(rowList);
            row++;
        }
        

        reader.close();

        return result;
    }

}
