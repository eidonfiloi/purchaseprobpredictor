package de.webtrekk.purchase.probability.predictor;

import java.io.IOException;
import java.util.ArrayList;

import de.webtrekk.algorithm.fader.ProbabilityForAllUsers;
import de.webtrekk.dfo.Calcfc;
import de.webtrekk.dfo.Cobyla;
import de.webtrekk.dfo.CobylaExitStatus;

public class MainPredictor {
    
    private static double rhobeg = 0.5;
    private static double rhoend = 1.0e-6;
    private static int iprint = 3;
    private static int maxfun = 3500;

   
    public static void main(String[] args) throws IOException {


        PreprocessMatrixData preprocess = new PreprocessMatrixData(457, 365);
        
        ArrayList<ArrayList<Integer>> views_train = preprocess.getViews_train();
        ArrayList<ArrayList<Integer>> buys_train = preprocess.getBuys_train();
        ArrayList<ArrayList<Integer>> views_train_cumulative = preprocess.getViews_train_cumulative();
        ArrayList<ArrayList<Integer>> buys_train_cumulative = preprocess.getBuys_train_cumulative();
        
        System.out.println("Preprocess ended");
        
        Calcfc calcfc = new ProbabilityForAllUsers(views_train, buys_train, views_train_cumulative, buys_train_cumulative);
        double[] params = {1,1,1,1,1,1};
        
        //System.out.println(calcfc.Compute(6, 5, params, new double[5]));
        CobylaExitStatus resultStatus = Cobyla.FindMinimum(calcfc, 6, 5, params, rhobeg, rhoend, iprint, maxfun);

    }

}
