# Purchase probability predictor


## Preprocessing

From raw.data.csv we extract the columns eid,date1,product_action. The PreprocessMatrix.java creates then 4 matrices 

* views
* buys
* views.cumulative
* buys.cumulative

containing per rows actions / cumulative actions for all eid for the whole time period (457 days). 

## Fader algorithm

The function ProbabilityForAllUsers.R is the main function subject to optimization using the optimx function from the optimx package. It takes paramteres the start parameters (r.v,r.t,mu,psi,k,pi) and the four matrix (views,buys,views.cumulative,buys.cumulative) restricted to the train period of time (e.g. 365 days):

* **optimx(params,ProbabilityForAllUsers,data=data,lower=lower,upper=upper,control=list(maximize=TRUE,all.methods=TRUE,trace=6,REPORT=1))** 

  
